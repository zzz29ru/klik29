<?php

function getMicroTime() {
	list($usec, $sec) = explode(' ', microtime());
	return ((float)$usec + (float)$sec);
}
$timeStart = getMicroTime();

session_start();

ini_set('max_execution_time',0);
mb_internal_encoding('UTF-8');

function connectDb() {
	if (!$db = @mysql_connect('localhost', 'root', '29klikmysql')) {die('В настоящий момент сервер базы данных недоступен, поэтому корректное отображение страницы невозможно.');}
	if (!@mysql_select_db('klik29_db', $db)) {die('В настоящий момент база данных недоступна, поэтому корректное отображение страницы невозможно.');}
	mysql_query("SET NAMES 'utf8'");
}
connectDb();

define('AC_DIR', dirname(__FILE__));
define('BASIC_URL', 'http://dom.29.ru/realty/sell/residential/secondary/');
define('URL_PARAMS', '?order=DateUpdate&dir=desc&PriceUnit=1&expand=0');
define('ITEMS_ON_PAGE', 50);

require_once(AC_DIR.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'RollingCurl.class.php');
require_once(AC_DIR.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'AngryCurl.class.php');
require_once(AC_DIR.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'phpQuery.class.php');

$AC = new AngryCurl('callbackList');

$AC->init_console();

$AC->load_proxy_list(
	AC_DIR.DIRECTORY_SEPARATOR.'import'.DIRECTORY_SEPARATOR.'proxy_list.txt',
	100,
	'http',
	'http://29.ru'
);
$AC->load_useragent_list(AC_DIR.DIRECTORY_SEPARATOR.'import'.DIRECTORY_SEPARATOR.'useragent_list.txt');

$response = file_get_contents(BASIC_URL.URL_PARAMS);
$html = phpQuery::newDocumentHTML($response, 'windows-1251');
$countEl = $html->find('#AdvCount');
$countFormated = trim($countEl->text());
$count = str_replace(',', '.', $countFormated);
$count = preg_replace('/[^x\d|*\.]/', '', $count);
$pages = intval(ceil($count/ITEMS_ON_PAGE));
$totalIds = 0;
// $items = array();
$onLastPage = $count - (ITEMS_ON_PAGE * ($pages - 1));

AngryCurl::add_debug_msg(
	"# ".$count." items, ".$pages." pages, ".ITEMS_ON_PAGE." items on page, ".$onLastPage." items on last page"
);

$statusPages = array();

for($i = 1; $i <= $pages; $i++) {
	$statusPages[$i] = BASIC_URL.$i.'.php'.URL_PARAMS;
}

while (!empty($statusPages)) {
	foreach($statusPages as $key => $value) {
		$AC->get($value);
	}
	$AC->execute(100);

	$AC->flush_requests();
}

mysql_query("UPDATE places SET sold = 'YES', sold_date = CURDATE() WHERE last_seen_date < (CURDATE() - INTERVAL 30 DAY)");

$timeEnd = getmicrotime();
$time = ($timeEnd - $timeStart)/60;

AngryCurl::add_debug_msg(
	$time." min"
);

unset($AC);


function callbackList($response, $info, $request) {
	global $AC, $pages, $count, $totalIds, $onLastPage, $statusPages, $items;

	$currentPage = str_replace(BASIC_URL, '', $info['url']);
	$currentPage = str_replace(URL_PARAMS, '', $currentPage);
	$currentPage = str_replace('.php', '', $currentPage);
	$currentPage = intval($currentPage);

	if (!$currentPage) {
		$currentPage = 1;
	}

	if($info['http_code'] == 200) {

		$html = phpQuery::newDocumentHTML($response, 'windows-1251');

		$itemsEl = $html->find('#advlist table.adv_table tr[id]');

		$itemsCount = count($itemsEl);

		if ((($currentPage != $pages) && ($itemsCount == ITEMS_ON_PAGE)) || (($currentPage == $pages) && ($itemsCount == $onLastPage))) {

			unset($statusPages[$currentPage]);

			$totalIds = $totalIds + $itemsCount;

			foreach ($itemsEl as $itemEl) {
				$item = array();
				$item['place_id'] = pq($itemEl)->attr('id');
				$item['place_id'] = intval(mb_substr($item['place_id'], 3));
				$item['url'] = BASIC_URL.'detail/'.$item['place_id'].'.php';
				$item['address'] = pq($itemEl)->find('td:eq(1) > a > b');
				$item['address'] = trim($item['address']->text());
				$item['address_hash'] = mb_strtolower(preg_replace('/[^a-zA-ZА-Яа-я0-9]/u', '', $item['address']));
				$item['district'] = pq($itemEl)->find('td:eq(1) .rl_note');
				$item['district'] = trim(mb_substr($item['district']->text(), 0, -3));
				$item['price'] = pq($itemEl)->find('td:eq(2)');
				$item['price'] = trim($item['price']->text());
				$item['price'] = ($item['price'] == 'договор.') ? 'договор.' : preg_replace('/[^0-9]/u', '', $item['price']);
				$item['square'] = pq($itemEl)->find('td:eq(3)');
				$item['square'] = trim($item['square']->text());
				$item['rooms'] = pq($itemEl)->find('td:eq(4)');
				$item['rooms'] = trim($item['rooms']->text());
				$item['floor'] = pq($itemEl)->find('td:eq(5)');
				$item['floor'] = trim($item['floor']->text());

				$floorsArr = explode('/', $item['floor']);
				$item['floor'] = trim($floorsArr[0]);
				$item['floors'] = trim($floorsArr[1]);

				$result = mysql_query("SELECT id FROM places WHERE address_hash = '".mysql_real_escape_string($item['address_hash'])."' AND price = '".mysql_real_escape_string($item['price'])."' AND square = '".mysql_real_escape_string($item['square'])."' AND floor = '".mysql_real_escape_string($item['floor'])."' AND sold = 'NO' LIMIT 1");
				if (!$result || !mysql_num_rows($result)) {
					mysql_query("INSERT INTO places(add_date, last_seen_date, place_id, url, address, address_hash, address_edit, district, price, price_edit, square, square_edit, rooms, floor, floor_edit, floors) VALUES(CURDATE(), CURDATE(), '".mysql_real_escape_string($item['place_id'])."', '".mysql_real_escape_string($item['url'])."', '".mysql_real_escape_string($item['address'])."', '".mysql_real_escape_string($item['address_hash'])."', '".mysql_real_escape_string($item['address'])."', '".mysql_real_escape_string($item['district'])."', '".mysql_real_escape_string($item['price'])."', '".mysql_real_escape_string($item['price'])."', '".mysql_real_escape_string($item['square'])."', '".mysql_real_escape_string($item['square'])."', '".mysql_real_escape_string($item['rooms'])."', '".mysql_real_escape_string($item['floor'])."', '".mysql_real_escape_string($item['floor'])."', '".mysql_real_escape_string($item['floors'])."')");
				}
				else {
					while ($place = mysql_fetch_assoc($result)) {
						mysql_query("UPDATE places SET last_seen_date = CURDATE() WHERE id = '".mysql_real_escape_string($place['id'])."'");
					}
				}

				unset($item);
				unset($result);
				unset($place);
			}
		}
	}

	unset($currentPage);
	unset($itemsCount);

    return;
}