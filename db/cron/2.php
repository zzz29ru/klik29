<?php

function getMicroTime() {
	list($usec, $sec) = explode(' ', microtime());
	return ((float)$usec + (float)$sec);
}
$timeStart = getMicroTime();

session_start();

ini_set('max_execution_time',0);
mb_internal_encoding('UTF-8');

function connectDb() {
	if (!$db = @mysql_connect('localhost', 'root', '29klikmysql')) {die('В настоящий момент сервер базы данных недоступен, поэтому корректное отображение страницы невозможно.');}
	if (!@mysql_select_db('klik29_db', $db)) {die('В настоящий момент база данных недоступна, поэтому корректное отображение страницы невозможно.');}
	mysql_query("SET NAMES 'utf8'");
}
connectDb();

define('AC_DIR', dirname(__FILE__));

require_once(AC_DIR.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'RollingCurl.class.php');
require_once(AC_DIR.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'AngryCurl.class.php');
require_once(AC_DIR.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'phpQuery.class.php');

$AC = new AngryCurl('callbackDetail');

$AC->init_console();

$AC->load_proxy_list(
	AC_DIR.DIRECTORY_SEPARATOR.'import'.DIRECTORY_SEPARATOR.'proxy_list.txt',
	100,
	'http',
	'http://29.ru'
);
$AC->load_useragent_list(AC_DIR.DIRECTORY_SEPARATOR.'import'.DIRECTORY_SEPARATOR.'useragent_list.txt');

$itemsDetails = array();
$itemsDetailUrls = array();

$result = mysql_query("SELECT id, url FROM places WHERE new = 'YES'");
if (!$result || !mysql_num_rows($result)) {}
else {
	while ($place = mysql_fetch_assoc($result)) {
		$itemsDetailUrls[$place['id']] = $place['url'].'?db_item_id='.$place['id'];
	}
}

while (!empty($itemsDetailUrls)) {
	foreach($itemsDetailUrls as $key => $value) {
		$AC->get($value);
	}
	$AC->execute(100);

	$AC->flush_requests();
}

foreach($itemsDetails as $itemDetail) {
	mysql_query("UPDATE places SET new = 'NO', other = '".mysql_real_escape_string($itemDetail['other'])."', phone = '".mysql_real_escape_string($itemDetail['phone'])."', details = '".mysql_real_escape_string($itemDetail['details'])."', type_house = '".mysql_real_escape_string($itemDetail['type_house'])."', series = '".mysql_real_escape_string($itemDetail['series'])."' WHERE id = ".$itemDetail['id']);
}


$timeEnd = getmicrotime();
$time = ($timeEnd - $timeStart)/60;

AngryCurl::add_debug_msg(
	$time." min"
);

unset($AC);

function callbackDetail($response, $info, $request) {

	global $AC, $itemsDetailUrls, $itemsDetails;

	$dbItemId = preg_replace("/.*?\?db_item_id\=/", '', $info['url']);
	$dbItemId = str_replace('/', '', $dbItemId);
	$dbItemId = intval($dbItemId);

	if($info['http_code'] == 200) {

		$html = phpQuery::newDocumentHTML($response, 'windows-1251');
		$check = $html->find('.detail_title');
		$check = $check->text();

		if ($check) {
			$item = array();
			$item['id'] = $dbItemId;
			$item['other'] = $html->find('div.rl_field:contains(Дополнительная информация)');
			$item['other'] = $item['other']->text();
			$item['other'] = str_ireplace('<br>', '\r\n', $item['other']);
			$item['other'] = str_ireplace('Дополнительная информация:', '', $item['other']);
			$item['other'] = trim($item['other']);
			$item['phone'] = $html->find('div.rl_field:contains(Контактная информация)');
			$item['phone'] = $item['phone']->text();
			$item['phone'] = str_ireplace('<br>', '\r\n', $item['phone']);
			$item['phone'] = str_ireplace('Контактная информация:', '', $item['phone']);
			$item['phone'] = trim($item['phone']);
			$item['details'] = $html->find('ul.review_right:contains(Детали)');
			$item['details'] = $item['details']->text();
			$item['details'] = str_ireplace('<br>', '\r\n', $item['details']);
			$item['details'] = str_ireplace('Детали:', '', $item['details']);
			$item['details'] = trim($item['details']);
			$item['type_house'] = $html->find('ul.review_left li:contains(Тип дома)');
			$item['type_house'] = $item['type_house']->text();
			$item['type_house'] = str_ireplace('<br>', '\r\n', $item['type_house']);
			$item['type_house'] = str_ireplace('Тип дома:', '', $item['type_house']);
			$item['type_house'] = trim($item['type_house']);
			$item['series'] = $html->find('ul.review_left li:contains(Серия)');
			$item['series'] = $item['series']->text();
			$item['series'] = str_ireplace('<br>', '\r\n', $item['series']);
			$item['series'] = str_ireplace('Серия:', '', $item['series']);
			$item['series'] = trim($item['series']);
			$itemsDetails[] = $item;
			unset($itemsDetailUrls[$dbItemId]);
		}
	}
    return;
}