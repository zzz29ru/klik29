<?
define("NOT_CHECK_PERMISSIONS", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("База недвижимости");

$tbl = array();
$tbl['address_edit']['name'] = 'Адрес';
$tbl['price_edit']['name'] = 'Цена';
$tbl['square_edit']['name'] = 'Общ. пл.';
$tbl['rooms']['name'] = 'Кол-во комнат';
$tbl['floor_edit']['name'] = 'Этаж';
$tbl['price_m']['name'] = 'Цена метр'; // общ цена / общ пл
$tbl['exclusivity']['name'] = 'Оценка';
$tbl['agency']['name'] = 'Агенство или частник';
$tbl['phone']['name'] = 'Контакты';
$tbl['other']['name'] = 'Доп. инф.';
$tbl['other']['textarea'] = 'yes';
$tbl['district']['name'] = 'Район';
$tbl['type_house']['name'] = 'Тип дома';
$tbl['series']['name'] = 'Серия';
$tbl['phone']['textarea'] = 'yes';
$tbl['details']['name'] = 'Детали';
$tbl['details']['textarea'] = 'yes';
$tbl['square_rooms']['name'] = 'Пл. комнат';
$tbl['kitchen']['name'] = 'Кухня';
$tbl['land_area']['name'] = 'Пл. участка';
$tbl['price_acre']['name'] = 'Цена за сотку';
$tbl['individual']['name'] = 'Комп./Частная';
$tbl['type']['name'] = 'Тип';
$tbl['type_lodging']['name'] = 'Тип помещ.';
$tbl['organization_developer']['name'] = 'Орг. застройщик';
$tbl['date_emplacement']['name'] = 'Дата заложения';
$tbl['date_construction_end']['name'] = 'Дата предп. оконч. застр.';
$tbl['name']['name'] = 'Имя';
$tbl['source']['name'] = 'Источник';
$tbl['balcony']['name'] = 'Лоджия/Балкон';
$tbl['straight']['name'] = 'Прямая/Цепочка';
$tbl['childs']['name'] = 'Кол. собств./Дети';
$tbl['price_other']['name'] = 'Сумма вся/Другое';
$tbl['year_built']['name'] = 'Год постр.';
$tbl['wear']['name'] = 'Износ';
$tbl['yk']['name'] = 'УК';
$tbl['heating']['name'] = 'Газ./Элек./Печное';
$tbl['overhaul']['name'] = 'Капрем. год (ожид)';
$tbl['replan']['name'] = 'Возм. переплан.';

$tbl_source = array();
$tbl_source['address']['name'] = 'Адр.';
$tbl_source['address_hash']['name'] = 'Хэш';
$tbl_source['price']['name'] = 'Цена';
$tbl_source['square']['name'] = 'Пл.';
$tbl_source['floor']['name'] = 'Этаж';

$tbl_date = array();
$tbl_date['add_date']['name'] = 'Добав.';
$tbl_date['sold_date']['name'] = 'Прод.';

$page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
$action = (isset($_GET['action'])) ? $_GET['action'] : 'show_all';
$order = (isset($_GET['order'])) ? $_GET['order'] : 'address_edit';
$source = (isset($_COOKIE['show_source']) && ($_COOKIE['show_source'] == 'yes')) ? 'yes' : 'no';

$actionGet = '?action='.$action;
$pageGet = '&page='.$page;
$orderGet = '&order='.$order;
$sourceGet = '&source='.$source;

$DB1 = new CDatabase;
$DB1->Connect($DBHost, $DB2Name, $DBLogin, $DBPassword);
$DB1->Query("SET NAMES 'utf8'");

$perPage = 200;

$whereQuery = '';
if ($action == 'show_sold') {
	$whereQuery = " WHERE sold = 'YES'";
}
elseif ($action == 'show_today') {
	$whereQuery = " WHERE add_date > (NOW() - INTERVAL 1 DAY)";
}
elseif ($action == 'show_uncheck') {
	$whereQuery = " WHERE checked = 'NO'";
}
elseif ($action == 'show_duplicates') {

	$whereQuery = " WHERE id = ''";

	$result = mysql_query("SELECT COUNT( * ) AS cnt, GROUP_CONCAT( id ) AS ids FROM places GROUP BY address, square, floor HAVING cnt >1");
	while ($place = mysql_fetch_assoc($result)) {
		$place['ids_arr'] = explode(',', $place['ids']);
		foreach ($place['ids_arr'] as $id) {
			$whereQuery .= " OR id = '".$id."'";
		}
	}
}

$startAt = $perPage * ($page - 1);

$result = $DB1->Query("SELECT COUNT(*) as total FROM places".$whereQuery);

while ($row = $result->Fetch()) {
	$totalPages = ceil($row['total'] / $perPage);
}

echo '<table class="overflow-y"><thead><tr>';
echo '<th></th>';

foreach ($tbl_date as $id => $val) {
	echo '<th>';
	echo ($order == $id) ? $val['name'] : '<a href="'.$actionGet.$sourceGet.'&order='.$id.'">'.$val['name'].'</a>';
	echo '</th>';
}

foreach ($tbl_source as $id => $val) {
	echo '<th class="line-source-th';
	if ($source == 'yes') {
		echo ' active';
	}
	echo '">';
	echo ($order == $id) ? $val['name'] : '<a href="'.$actionGet.$sourceGet.'&order='.$id.'">'.$val['name'].'</a>';
	echo '</th>';
}

foreach ($tbl as $id => $val) {
	echo '<th>';
	echo ($order == $id) ? $val['name'] : '<a href="'.$actionGet.$sourceGet.'&order='.$id.'">'.$val['name'].'</a>';
	echo '</th>';
}

echo '<th><span class="glyphicon glyphicon-trash"></span></th>';
echo '</tr></thead><tbody>';

$orderQuery = '';
if ($action != 'show_duplicates') {
	$orderQuery = " ORDER BY ".$order." REGEXP '^-?[0-9\.]+$' AND LENGTH(".$order.") - LENGTH(REPLACE(".$order.", '.', '')) < 2 DESC, CAST(".$order." AS UNSIGNED), ".$order;
}
else {
	$orderQuery = " ORDER BY address_hash ASC, square ASC, floor ASC";
}

$result = $DB1->Query("SELECT * FROM places".$whereQuery.$orderQuery." LIMIT ".$startAt.", ".$perPage);

while ($place = $result->Fetch()) {

	echo '<tr data-pk="'.$place['id'].'" class="';
	if ($place['active'] == 'NO') {
		echo 'line-disable ';
	}
	if ($action == 'show_all') {
		$cur_date = new DateTime();
		$cur_date->modify('-1 day');
		if ($place['add_date'] > $cur_date->format('Y-m-d H:i:s')) {
			echo 'today ';
		}
	}
	echo '">';
	echo '<th>';
	echo '<a class="item-checked';
	if ($place['checked'] == 'YES') {
		echo ' active';
	}
	echo  '" href="#" data-pk="'.$place['id'].'"><span class="glyphicon glyphicon-ok"></span></a>';
	echo '<a class="item-enable';
	if ($place['active'] == 'YES') {
		echo ' active';
	}
	echo  '" href="#" data-pk="'.$place['id'].'"><span class="glyphicon glyphicon-off"></span></a>';
	echo '<a class="item-sold';
	if ($place['sold'] == 'YES') {
		echo ' active';
	}
	echo '" href="#" data-pk="'.$place['id'].'"><span class="glyphicon glyphicon-home"></span></a>';
	echo '<a class="item-go" target="_blank" href="'.$place['url'].'"><span class="glyphicon glyphicon-share"></span></a>';
	echo '</th>';

	foreach ($tbl_date as $id => $val) {
		echo '<td class="line-'.$id.'">';
		echo '<div>';
		echo $place[$id] ? date('d.m.Y', strtotime($place[$id])) : '—';
		echo '</div>';
		echo '</td>';
	}

	foreach ($tbl_source as $id => $val) {
		echo '<td class="line-'.$id.' line-source-highlight';
		if ($source == 'yes') {
			echo ' active';
		}
		echo '">';
		echo '<div>';
		echo '<span class="editable" data-type="';
		echo $tbl_source[$id]['textarea'] ? 'textarea' : 'text';
		echo '" data-name="'.$id.'" data-pk="'.$place['id'].'">';
		echo $place[$id] ? $place[$id] : '—';
		echo '</span>';
		echo '</div>';
		echo '</td>';
	}

	foreach ($tbl as $id => $val) {
		if ($id == 'price_m') {
			echo '<td class="line-'.$id.'">';
			echo '<div>';
			echo ($place['price_edit'] && $place['square_edit']) ? round($place['price_edit']/$place['square_edit']) : '—';
			echo '</div>';
			echo '</td>';
		}
		else {
			echo '<td class="line-'.$id.'">';
			echo '<div>';
			echo '<span class="editable" data-type="';
			echo $tbl[$id]['textarea'] ? 'textarea' : 'text';
			echo '" data-name="'.$id.'" data-pk="'.$place['id'].'">';
			echo $place[$id] ? $place[$id] : '—';
			echo '</span>';
			echo '</div>';
			echo '</td>';
		}
	}
	echo '<td><a class="item-delete" href="#" data-pk="'.$place['id'].'"><span class="glyphicon glyphicon-trash"></span></a></td>';
}

echo '</tbody></table>';
echo '<div class="footer">';
if ($totalPages > 1) {
	echo '<div class="pagination">';
	echo '</div>';
}
echo '<div class="controls">';
echo '<div class="show-source">';
echo '<ul>';
echo '<li';
echo ($source == 'yes') ? ' class="active"' : '';
echo '><a href="#">Исходные данные</a></li>';
echo '</ul>';
echo '</div>';
echo '<div class="switcher"><ul><li';
echo ($action == 'show_all') ? ' class="active"' : '';
echo '><a href="?action=show_all'.$sourceGet.'">Все</a></li><li';
echo ($action == 'show_sold') ? ' class="active"' : '';
echo '><a href="?action=show_sold'.$sourceGet.'">Проданные</a></li><li';
echo ($action == 'show_uncheck') ? ' class="active"' : '';
echo '><a href="?action=show_uncheck'.$sourceGet.'">Непроверенные</a></li><li';
echo ($action == 'show_duplicates') ? ' class="active"' : '';
echo '><a href="?action=show_duplicates'.$sourceGet.'">Дубли</a></li><li';
echo ($action == 'show_today') ? ' class="active"' : '';
echo '><a href="?action=show_today'.$sourceGet.'">За сегодня</a></li>
<li><div class="js-btn">
   <div class="js-browse">
      <a href="#"><span class="glyphicon glyphicon-floppy-open"></span></a>
      <input type="file" name="filedata">
   </div>
   <div class="js-upload" style="display: none">
      <a href="#">Загрузка (<span class="js-size"></span>)</a>
   </div>
</div>
<div class="js-done" style="display: none">
<a href="#"><span class="glyphicon glyphicon-floppy-saved"></span></a>
</div>
</li>

</ul></div>';
echo '</div>';
echo '</div>';

$DB1->Disconnect();

?>
<script>
$(document).ready(function() {
	$('.pagination').pagination({
		currentPage: <?=$page?>,
		pages: <?=$totalPages?>,
		hrefTextPrefix: '<?=$actionGet.$sourceGet.$orderGet?>&page=',
		hrefTextSuffix: '',
		prevText: '&laquo;',
		nextText: '&raquo;'
	});
});
</script>
<?

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>