<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<?
$rsSites = CSite::GetByID(SITE_ID);
$arSite = $rsSites->Fetch();
?>
<title><?$APPLICATION->ShowTitle()?> | <?=$arSite["NAME"]?></title>
<script>
    window.FileAPI = {
          debug: false // debug mode
        , staticPath: '<?=SITE_TEMPLATE_PATH?>/js/jquery.fileapi/FileAPI/' // path to *.swf
    };
</script>
<?
	$APPLICATION->ShowHead(true);

	$APPLICATION->AddHeadScript("https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js");
	$APPLICATION->AddHeadScript("http://cdnjs.cloudflare.com/ajax/libs/jquery-throttle-debounce/1.1/jquery.ba-throttle-debounce.min.js");

	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.poshytip.min.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.editable/jquery-editable/js/jquery-editable-poshytip.min.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.simplePagination/jquery.simplePagination.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.floatThead.min.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.cookie.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.fileapi/FileAPI/FileAPI.min.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.fileapi/FileAPI/FileAPI.exif.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.fileapi/jquery.fileapi.min.js");

	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/scripts.js");

	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/normalize.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/component.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/js/jquery.editable/jquery-editable/css/jquery-editable.css");
?>
</head>
<body>