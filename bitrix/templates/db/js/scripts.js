$(document).ready(function() {
	$.fn.editable.defaults.url = 'edit/';
	$.fn.editable.defaults.emptytext = '—';
	$.fn.editableform.buttons = '<button type="submit" class="editable-submit"><span class="glyphicon glyphicon-ok"></span></button><button type="button" class="editable-cancel"><span class="glyphicon glyphicon-remove"></span></button>';
	$('.editable').click(function(e) {
	    e.stopPropagation();
	    $(this).editable('show');
	    $(this).editable('active');
	});
	$('.editable').on('shown', function(e, editable) {
	    if ($(this).html() == '—') {
	    	editable.input.$input.val('');
	    }
	});
	var $table = $('table');
	$table.floatThead();
	$('.show-source a').click(function(e){
		e.preventDefault();
		var $this = $(this);
		if ($this.parents('li').hasClass('active')) {
			$.removeCookie('show_source', { path: '/' });
		}
		else {
			$.cookie('show_source', 'yes', { expires: 30, path: '/' });
		}
		$('.show-source li').toggleClass('active');
		$('.line-source-th').toggleClass('active');
		$('.line-source-highlight').toggleClass('active');
		$table.trigger('reflow');
	});
	$('.item-checked').click(function(e){
		e.preventDefault();
		var $this = $(this);
		if ($this.hasClass('active')) {
			$.post($.fn.editable.defaults.url, {
				uncheck: $this.data('pk')
			});
		}
		else {
			$.post($.fn.editable.defaults.url, {
				check: $this.data('pk')
			});
		}
		$('.item-checked[data-pk="'+$this.data('pk')+'"]').toggleClass('active');
	});
	$('.item-enable').click(function(e){
		e.preventDefault();
		var $this = $(this);
		if ($this.hasClass('active')) {
			$.post($.fn.editable.defaults.url, {
				disable: $this.data('pk')
			});
		}
		else {
			$.post($.fn.editable.defaults.url, {
				enable: $this.data('pk')
			});
		}
		$('.item-enable[data-pk="'+$this.data('pk')+'"]').toggleClass('active').parents('tr').toggleClass('line-disable');
	});
	$('.item-sold').click(function(e){
		e.preventDefault();
		var $this = $(this);
		if ($this.hasClass('active')) {
			$.post($.fn.editable.defaults.url, {
				unsold: $this.data('pk')
			});
		}
		else {
			$.post($.fn.editable.defaults.url, {
				sold: $this.data('pk')
			});
		}
		$('.item-sold[data-pk="'+$this.data('pk')+'"]').toggleClass('active');
	});
	$('.item-delete').click(function(e){
		e.preventDefault();
		var $this = $(this);

		$.post($.fn.editable.defaults.url, {
			'delete': $this.data('pk')
		});
		$('tr[data-pk="'+$this.data('pk')+'"]').addClass('line-delete');
	});
	$('.js-btn').fileapi({
	   url: $.fn.editable.defaults.url+'?action=upload',
	   multiple: false,
	   maxSize: 20 * FileAPI.MB,
	   autoUpload: true,
	   elements: {
	      size: '.js-size',
	      active: { show: '.js-upload', hide: '.js-browse' },
	   },
	   onFileComplete: function() {
	   	$('.js-btn').hide();
	   	$('.js-done').show();
	   }
	});
});