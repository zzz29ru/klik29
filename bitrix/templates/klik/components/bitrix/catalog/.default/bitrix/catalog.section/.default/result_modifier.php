<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
GarbageStorage::set('SECTION_ID', $arResult["ID"]);
foreach ($arResult['ITEMS'] as $key => $arItem)
{
	$itemPictures = array();
	$itemPictures = array(
		$arItem["PREVIEW_PICTURE"]["ID"]
	);

	$arResult['ITEMS'][$key]["RESIZE_PICTURES"]["PREVIEW_PICTURE"] = PxFunc::ResizePicture($itemPictures, 130, 85);
}
?>