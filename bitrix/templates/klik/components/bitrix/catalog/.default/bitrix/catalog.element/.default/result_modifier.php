<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
GarbageStorage::set('SECTION_ID', $arResult["SECTION"]["ID"]);
$arPictures = array();
foreach($arResult["PROPERTIES"]["PHOTOS"]["VALUE"] as $photo)
{
	$itemPictures = array(
		$photo
	);

	$arPictures[] = array(
		"PREVIEW_PICTURE" => PxFunc::ResizePicture($itemPictures, 47, 35),
		"DETAIL_PICTURE" => PxFunc::ResizePicture($itemPictures, 620, 360)
	);
}

$arResult["RESIZE_PICTURES"] = $arPictures;
?>