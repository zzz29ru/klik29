<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if($arResult["ITEMS"]):?>

<?
foreach($arResult["ITEMS"] as $cell=>$arElement):
	$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CATALOG_ELEMENT_DELETE_CONFIRM')));
?>

<div class="list_properties" id="<?=$this->GetEditAreaId($arElement['ID']);?>">
	<div class="title_property2">
	<h2><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["PROPERTIES"]["CITY"]["VALUE"]?>, <?=$arElement["NAME"]?></a></h2>
	</div>
	<div class="clear"></div>
	<div class="list_img">
	<?if(is_array($arElement["PREVIEW_PICTURE"])):?>
		<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img border="0" src="<?=$arElement["RESIZE_PICTURES"]["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" /></a>
	<?else:?>
		<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img border="0" src="<?=SITE_TEMPLATE_PATH?>/images/no_img.jpg" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" /></a>
	<?endif;?>
	</div>
	<div class="list_text">
	<?if($arElement["PROPERTIES"]["PRICE"]["VALUE"]):?><strong><?=number_format($arElement["PROPERTIES"]["PRICE"]["VALUE"], 0, '.', ' ');?></strong> <?=$arParams["CURRENCY"]?><br /><?endif;?>
	<?if($arElement["PROPERTIES"]["AREA"]["VALUE"]):?>Площадь: <?=$arElement["PROPERTIES"]["AREA"]["VALUE"]?> <?endif;?>
	<?if($arElement["PROPERTIES"]["ROOMS"]["VALUE"]):?>| Комнат: <?=$arElement["PROPERTIES"]["ROOMS"]["VALUE"]?><?endif;?>
	<br />
	<br />
	<?if($arElement["PROPERTIES"]["CONDITION"]["VALUE"]):?>Состояние: <?=$arElement["PROPERTIES"]["CONDITION"]["VALUE"]?><br><?endif;?>
	<?if($arElement["PROPERTIES"]["AGENT"]["VALUE"]):?><span class="blue">Агент: <?=$arElement["PROPERTIES"]["AGENT"]["VALUE"]?></span><?endif;?>
	</div>
	<div class="clear"></div>
</div>

<?endforeach;?>
<div class="clear"></div>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
<?=$arResult["NAV_STRING"]?>
<?endif;?>

<?else:?>
<p>Объектов нет</p>
<?endif;?>