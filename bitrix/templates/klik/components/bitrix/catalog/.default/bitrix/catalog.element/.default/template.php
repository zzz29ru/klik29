<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?$APPLICATION->SetTitle($arResult["PROPERTIES"]["CITY"]["VALUE"].", ".$arResult["NAME"]);?>

<?if(!empty($arResult["RESIZE_PICTURES"])):?>

<div id="container-slider">
<ul id="slideshow_detail">

<?foreach ($arResult["RESIZE_PICTURES"]  as $apPhoto):?>
<li>
	<h3></h3>
	<span><?=$apPhoto["DETAIL_PICTURE"]["SRC"]?></span>
	<p />
	<img src="<?=$apPhoto["PREVIEW_PICTURE"]["SRC"]?>" alt="thumb" />
</li>
<?endforeach?>

</ul>
<div id="wrapper">
	<div id="fullsize">
		<div id="imgprev" class="imgnav" title="Previous Image"></div>
		<div id="imglink"></div>
		<div id="imgnext" class="imgnav" title="Next Image"></div>
		<div id="image"></div>
		<div id="information">
			<h3></h3>
			<p />
		</div>
	</div>
	<div id="thumbnails">
		<div id="slideleft" title="Slide Left"></div>
		<div id="slidearea">
			<div id="slider"></div>
		</div>
		<div id="slideright" title="Slide Right"></div>
	</div>
</div>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/compressed.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/script.js"></script>
<script type="text/javascript">
<!--

$('slideshow_detail').style.display='none';
$('wrapper').style.display='block';
var slideshow_detail=new TINY.slideshow_detail("slideshow_detail");
window.onload=function(){
	slideshow_detail.auto=true;
	slideshow_detail.speed=5;
	slideshow_detail.link="linkhover";
	slideshow_detail.info="information";
	slideshow_detail.thumbs="slider";
	slideshow_detail.left="slideleft";
	slideshow_detail.right="slideright";
	slideshow_detail.scrollSpeed=4;
	slideshow_detail.spacing=25;
	slideshow_detail.active="#fff";
	slideshow_detail.init("slideshow_detail","image","imgprev","imgnext","imglink");
}
//-->
</script>
</div><!-- end content-slider -->

<div class="clear"><br /><br /></div>

<?endif;?>

<h2 class="underline">Характеристики</h2>

<div id="property-detail">
<ul class="box_text">
	<li><span class="left"><?=$arResult["PROPERTIES"]["PRICE"]["NAME"]?></span>	<span class="right"><?=($arResult["PROPERTIES"]["PRICE"]["VALUE"])?"<strong>".number_format($arResult["PROPERTIES"]["PRICE"]["VALUE"], 0, '.', ' ')."</strong> ".$arParams["CURRENCY"]:"-"?></span></li>
</ul>
<div class="one_half">
	<ul class="box_text">
		<li><span class="left"><?=$arResult["PROPERTIES"]["AREA"]["NAME"]?></span> <?=($arResult["PROPERTIES"]["AREA"]["VALUE"])?$arResult["PROPERTIES"]["AREA"]["VALUE"]:"-"?></li>
		<li><span class="left"><?=$arResult["PROPERTIES"]["ROOMS"]["NAME"]?></span> <?=($arResult["PROPERTIES"]["ROOMS"]["VALUE"])?$arResult["PROPERTIES"]["ROOMS"]["VALUE"]:"-"?></li>
		<li><span class="left"><?=$arResult["PROPERTIES"]["FLOOR"]["NAME"]?></span> <?=($arResult["PROPERTIES"]["FLOOR"]["VALUE"])?$arResult["PROPERTIES"]["FLOOR"]["VALUE"]:"-"?></li>
	</ul>
</div>
<div class="one_half last">
	<ul class="box_text">
		<li><span class="left"><?=$arResult["PROPERTIES"]["BATHROOM"]["NAME"]?></span> <?=($arResult["PROPERTIES"]["BATHROOM"]["VALUE"])?$arResult["PROPERTIES"]["BATHROOM"]["VALUE"]:"-"?></li>
		<li><span class="left"><?=$arResult["PROPERTIES"]["BALCONY"]["NAME"]?></span> <?=($arResult["PROPERTIES"]["BALCONY"]["VALUE"])?$arResult["PROPERTIES"]["BALCONY"]["VALUE"]:"-"?></li>
		<li><span class="left"><?=$arResult["PROPERTIES"]["ELEVATOR"]["NAME"]?></span> <?=($arResult["PROPERTIES"]["ELEVATOR"]["VALUE"])?$arResult["PROPERTIES"]["ELEVATOR"]["VALUE"]:"-"?></li>
	</ul>
</div>
	<ul class="box_text">
		<li><span class="left"><?=$arResult["PROPERTIES"]["DESC"]["NAME"]?></span>	<span class="right"><?=($arResult["PROPERTIES"]["DESC"]["VALUE"]["TEXT"])?$arResult["PROPERTIES"]["DESC"]["VALUE"]["TEXT"]:"-"?></span></li>
		<li><span class="left"><?=$arResult["PROPERTIES"]["AGENT"]["NAME"]?></span>	<span class="right"><?=($arResult["PROPERTIES"]["AGENT"]["VALUE"])?"<span class='blue'>".$arResult["PROPERTIES"]["AGENT"]["VALUE"]."</span>":"-"?></span></li>
	</ul>
</div>

<?if($arResult["PROPERTIES"]["MAP"]["VALUE"]):
$map_val = explode(",", $arResult["PROPERTIES"]["MAP"]["VALUE"]);
?>

<h2 class="underline">Объект на карте</h2>

<?$APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view",
	"",
	Array(
		"INIT_MAP_TYPE" => "MAP",
		"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:".$map_val[0].";s:10:\"yandex_lon\";d:".$map_val[1].";s:12:\"yandex_scale\";i:16;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:".$map_val[1].";s:3:\"LAT\";d:".$map_val[0].";s:4:\"TEXT\";s:0:\"\";}}}",
		"MAP_WIDTH" => "620",
		"MAP_HEIGHT" => "400",
		"CONTROLS" => array("ZOOM", "MINIMAP", "TYPECONTROL", "SCALELINE"),
		"OPTIONS" => array("ENABLE_SCROLL_ZOOM", "ENABLE_DBLCLICK_ZOOM", "ENABLE_DRAGGING"),
		"MAP_ID" => ""
	),
false
);?>
<?endif;?>