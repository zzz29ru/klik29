<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>








<!-- BEGIN SLIDE -->
<div id="slider_container">
<div id="slideshow_navigation">
<div id="pager"></div>
</div><!-- end slideshow navigation -->
	<div id="slideshow">

		<?foreach($arResult["ITEMS"] as $arItem):?>

		<div class="cycle">
			<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="" />
			<div class="farme-slide-text">
				<ul class="slide-text">
					<li><span class="left">Адрес:</span> <?=$arItem["PROPERTIES"]["CITY"]["VALUE"]?>, <?=$arItem["NAME"]?></li>
					<li><span class="left"><?=$arItem["PROPERTIES"]["AREA"]["NAME"]?></span> <?=($arItem["PROPERTIES"]["AREA"]["VALUE"])?$arItem["PROPERTIES"]["AREA"]["VALUE"]:"-"?></li>
					<li><span class="left"><?=$arItem["PROPERTIES"]["ROOMS"]["NAME"]?></span> <?=($arItem["PROPERTIES"]["ROOMS"]["VALUE"])?$arItem["PROPERTIES"]["ROOMS"]["VALUE"]:"-"?></li>
					<li><span class="left"><?=$arItem["PROPERTIES"]["FLOOR"]["NAME"]?></span> <?=($arItem["PROPERTIES"]["FLOOR"]["VALUE"])?$arItem["PROPERTIES"]["FLOOR"]["VALUE"]:"-"?></li>
					<li><span class="left"><?=$arItem["PROPERTIES"]["BATHROOM"]["NAME"]?></span> <?=($arItem["PROPERTIES"]["BATHROOM"]["VALUE"])?$arItem["PROPERTIES"]["BATHROOM"]["VALUE"]:"-"?></li>
					<li><span class="left"><?=$arItem["PROPERTIES"]["BALCONY"]["NAME"]?></span> <?=($arItem["PROPERTIES"]["BALCONY"]["VALUE"])?$arItem["PROPERTIES"]["BALCONY"]["VALUE"]:"-"?></li>
				</ul>
				<div class="frame-price">
					<div class="slider-button"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>">подробнее</a></div>
					<div class="slider-price"><?=($arItem["PROPERTIES"]["PRICE"]["VALUE"])?number_format($arItem["PROPERTIES"]["PRICE"]["VALUE"], 0, '.', ' ')." ".$arParams["CURRENCY"]:"-"?></div>
				</div>
			</div>
		</div><!-- end cycle -->

		<?endforeach;?>

	</div><!-- end #slideshow -->
</div><!-- end #slide -->
<!-- END OF SLIDE -->






