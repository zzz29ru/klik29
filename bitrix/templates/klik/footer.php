
				</div><!-- end #content -->

			<div class="sidebar_right">
				<div class="sidebar">

					<div class="bg_gray">
						<form action="/search/">
							  <?$APPLICATION->IncludeComponent("bitrix:search.suggest.input", ".default", array(
								"NAME" => "q",
								"INPUT_SIZE" => "30",
								"DROPDOWN_SIZE" => "10"
								),
								false,
								array("HIDE_ICONS" => "Y")
							  );?>
							  <input type="submit" class="button" value="Найти" />
						</form>
					</div>

					<ul>
						<li class="widget-container widget_categories">
							<?$APPLICATION->IncludeComponent(
								"bitrix:main.include",
								"sidebar",
								array(
									"AREA_FILE_SHOW" => "sect",
									"AREA_FILE_SUFFIX" => "sidebar",
									"AREA_FILE_RECURSIVE" => "Y",
									"EDIT_TEMPLATE" => ""
								),
								false,
								array("HIDE_ICONS" => "Y")
							);?>
							</ul>
						</li>
					</ul>
				</div><!-- end #sidebar -->
			</div><!-- end #sidebar_right -->

			<div class="clear"></div>
		</div><!-- end #maincontent -->
	</div><!-- end #centercolumn -->


	<div id="bottom-container">
		<div class="centercolumn">

			<div id="copyright">© <?=date("Y")?> <a href="/">Клик</a> - агенство недвижимости</div>
			<!-- end #foot -->
		</div><!-- end #centercolumn -->
		<div class="clear"></div>
	</div>
</body>
</html>
