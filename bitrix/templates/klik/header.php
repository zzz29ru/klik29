<!DOCTYPE html>
<head>
<meta charset="utf-8">
<?
$rsSites = CSite::GetByID(SITE_ID);
$arSite = $rsSites->Fetch();
?>
<title><?$APPLICATION->ShowTitle()?> | <?=$arSite["NAME"]?></title>
<?
	$APPLICATION->ShowHead(true);

	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery-1.4.2.min.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/query.cycle.all.min.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/hoverIntent.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/superfish.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/supersubs.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.cycle.all.min.js");

	$APPLICATION->SetAdditionalCSS("http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic&subset=latin,cyrillic,latin-ext,cyrillic-ext");


?>

<script type="text/javascript">
 var $jts = jQuery.noConflict();
    $jts(document).ready(function(){
        $jts("ul.sf-menu").supersubs({
		minWidth		: 9,		// requires em unit.
		maxWidth		: 25,		// requires em unit.
		extraWidth		: 0			// extra width can ensure lines don't sometimes turn over due to slight browser differences in how they round-off values
                               // due to slight rounding differences and font-family
        }).superfish();  // call supersubs first, then superfish, so that subs are
                         // not display:none when measuring. Call before initialising
                         // containing tabs for same reason.
    });

</script>
<script type="text/javascript">
 var $jts = jQuery.noConflict();
    $jts(document).ready(function(){

		//Slider
         $jts('#slideshow').cycle({
            timeout: 5000,  // milliseconds between slide transitions (0 to disable auto advance)
            fx:      'fade', // choose your transition type, ex: fade, scrollUp, shuffle, etc...
            pager:   '#pager',  // selector for element to use as pager container
            pause:   0,	  // true to enable "pause on hover"
            pauseOnPagerHover: 0 // true to pause when hovering over pager link
        });
     });
</script>

<body>

<?$APPLICATION->ShowPanel();?>

	<div id="top-container">
		<div class="centercolumn">
		<div id="header">
			<div id="logo">
				<a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" alt="" /></a>
			</div><!-- end #logo -->
			<div id="navigation">
				<?$APPLICATION->IncludeComponent("bitrix:menu", "top", Array(
					"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
					"MAX_LEVEL" => "2",	// Уровень вложенности меню
					"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
					"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
					"MENU_CACHE_TYPE" => "A",	// Тип кеширования
					"MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
					"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
					"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
					),
					false,
					array(
						"HIDE_ICONS" => "Y",
						"ACTIVE_COMPONENT" => "Y"
					)
				);?>
			</div><!-- end #navigation-->
			<div class="clr"></div>
		</div><!-- end #header -->
		</div><!-- end #centercolumn -->
	</div><!-- end #top-container -->


	<div class="centercolumn">

		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"topbar",
			array(
				"AREA_FILE_SHOW" => "page",
				"AREA_FILE_SUFFIX" => "topbar",
				"AREA_FILE_RECURSIVE" => "Y",
				"EDIT_TEMPLATE" => ""
			),
			false,
			array("HIDE_ICONS" => "Y")
		);?>

		<div id="maincontent">
			<div id="content">
			<h2 class="underline"><?$APPLICATION->ShowTitle(false);?></h2>