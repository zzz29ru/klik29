<?
Class GarbageStorage{
   private static $storage = array();
   public static function set($name, $value){ self::$storage[$name] = $value;}
   public static function get($name){ return self::$storage[$name];}
}

class PxFunc
{
	/**
	 * ArKeyUpper
     * Преобразование ключей массива в верхний регистр
     *
     * @param array
     * @return boolean
     */
	static function ArKeyUpper(&$array)
    {
        foreach($array as &$value)
        {
            if(is_array($value))
            {
                PxFunc::ArKeyUpper($value);
            }
        }
        $array = array_change_key_case($array, CASE_UPPER);

        return $array;
    }

    /**
     * ArKeyLower
     * Преобразование ключей массива в нижний регистр
     *
     * @param array
     * @return boolean
     */
    static function ArKeyLower(&$array)
    {
        foreach($array as &$value)
        {
            if(is_array($value))
            {
                PxFunc::ArKeyLower($value);
            }
        }
        $array = array_change_key_case($array, CASE_LOWER);

        return $array;
    }

    /**
	 * SetDebug
     * Запись в журнал сообщение отладки
     *
     * @param string, array
     * @param boolean
     * @return boolean
     */
    static function SetDebug($message, $array = false)
    {
        if ($array === true)
            $message = var_export($message, true);

        syslog(LOG_DEBUG, $message);

        return true;
    }

    /**
	 * ResizePicture
     * Изменение размеров изображений для предварительного просмотра
     *
     * @param array
     * @param integer
     * @param integer
     * @return array
     */
    static function ResizePicture($pictures, $width, $height, $proportional = false)
    {
        foreach ($pictures as $picture)
        {
            if(count($picture) > 0 && $picture !== NULL && $picture !== false)
            {
                $type = ($proportional)? BX_RESIZE_IMAGE_PROPORTIONAL_ALT : BX_RESIZE_IMAGE_EXACT;
                $output = CFile::ResizeImageGet(
                    $picture,
                    array(
						"width" => intval($width),
						"height" => intval($height)
					),
                    $type,
                    true
                );

                break;
            }
        }

        return PxFunc::ArKeyUpper($output);
    }

    /**
     * MbUcfirst
     * Преобразование первого символа в верхний регистр
     *
     * @param string
     * @param string
     * @return string
     */
    static function MbUcfirst($str, $encoding = "UTF-8")
    {
        $str = mb_ereg_replace('^[\ ]+', '', $str);
        $str = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding).
               mb_substr($str, 1, mb_strlen($str), $encoding);
        return $str;
    }


    /**
     * FormatMonth
     * Конвертация формата месяца с 02 в Февраля
     *
     * @param string
     * @return string
     */
    function FormatMonth($m)
    {
        $array = array(
            "01" => "Января",
            "02" => "Февраля",
            "03" => "Марта",
            "04" => "Апреля",
            "05" => "Мая",
            "06" => "Июня",
            "07" => "Июля",
            "08" => "Августа",
            "09" => "Сентября",
            "10" => "Октября",
            "11" => "Ноября",
            "12" => "Декабря"
        );

        $output = $array[$m];

        return $output;
    }
    /**
     * FormatDate
     * Конвертация формата даты с 04.11.2008 в 04 Ноября, 2008
     *
     * @param string
     * @return string
     */
    function FormatDate($date)
    {
        $arDate = explode(".", $date);
        $date = ($arDate[0] < 10) ? substr($arDate[0], 1) : $arDate[0];

        $output = $date." ".self::FormatMonth($arDate[1]).", ".$arDate[2];

        return $output;
    }

    /**
     * FormatText
     * Форматирование текста для предложений
     *
     * @param string
     * @return string
     */
    function FormatText($string, $type = "ul")
    {
        $arString = explode("<br />", nl2br($string));
        foreach ($arString as $i => $item)
        {
            $item = str_replace("\r\n", "", $item);
            $item = str_replace("\n","",$item);
            $arString[$i] = $item;
        }
        $arString = array_diff($arString, array(null));


        switch ($type) {
            case "ul":
                $output = "<ul>";
                foreach ($arString as $item)
                    $output .= "<li>".trim($item)."</li>";
                $output .= "</ul>";
                break;
            case "p":
                foreach ($arString as $item)
                    $output .= "<p>".trim($item)."</p>";
                break;
            case "clear":
                foreach ($arString as $item)
                    $output .= trim($item)."\n";
                break;
            case "menu":
                    foreach ($arString as $item)
                    {
                        if(preg_match('/(.+)(?:\s-\s|\s|-)+([\.\,\d]+)\s*(?:рублей|руб|р)+\.*/i', $item, $matches))
                        {
                            $oText = preg_replace('/(.+)(?:\s\-)$/', "$1", $matches[1]);
                            $output .= "<p class=\"menu-line\">";
                                $output .= "<span>".trim($oText)."</span>";
                                $output .= "<strong>".trim($matches[2])." р</strong>";
                            $output .= "</p>";
                        } elseif(preg_match('/.+\s.*[\/\.\,\d]+.*\s*(?:гр|мл)*\.*.*/i', $item, $matches)) {
                            $output .= "<p class=\"menu-line\">";
                                $output .= "<span>".trim($matches[0])."</span>";
                            $output .= "</p>";
                        } else {
                            $output .= "<h3>".trim($item)."</h3>";
                        }
                    }
                break;
            case "desc":
                foreach ($arString as $item)
                    $output .= ($output)? ", ".trim($item): trim($item);
                break;
            default:
                $output = $string;
        }

         return $output;
    }

     /**
     * Declination
     * Изменение окончания у слова
     *
     * @param string
     * @param array
     * @return string
     */
    function Declination($number, $titles)
    {
        $cases = array (2, 0, 1, 1, 1, 2);
        return $titles[ ($number%100>4 && $number%100<20)? 2 : $cases[min($number%10, 5)] ];
    }

    /**
     * FormatHours
     * Форматирование времени на сегодняшний день
     *
     * @param string
     * @return string
     */
    function FormatHours($string, $prefix = "")
    {
        $arTime = split(";", mb_strtolower(trim($string)));
        $arKeyWeek = array("пн", "вт", "ср", "чт", "пт", "сб", "вс");
        $arWeek = array("пн" => "", "вт" => "", "ср" => "", "чт" => "", "пт" => "", "сб" => "", "вс" => "");

        foreach ($arTime as $time)
        {
            $arTimeString = split(" ", trim($time));

            $arTimeString[0] = str_replace("–", "-", $arTimeString[0]);
            $arWeekString = split("-|–", $arTimeString[0]);

            if(count($arWeekString) > 1)
            {
                $begin = false;
                if( array_key_exists($arWeekString[0], $arWeek) &&
                    array_key_exists($arWeekString[1], $arWeek) )
                    foreach ($arWeek as $key => $value)
                    {
                        if($arWeekString[0] == $key)
                            $begin = true;

                        if($begin)
                            $arWeek[$key] = $arTimeString[1];

                        if($arWeekString[1] == $key)
                            $begin = false;
                    }
                else
                    $errors = true;
            }
            else
                if(array_key_exists($arWeekString[0], $arWeek))
                    $arWeek[$arWeekString[0]] = $arTimeString[1];
                else
                    $errors = true;

            $output .= ($output)? "; ".trim($time) : trim($time);
        }

        if( ! $errors)
        {
            $week = date("L", date("Y-m-d"));
            $arTimeWork = split("-|–", $arWeek[$arKeyWeek[$week]]);
            $output = trim($prefix." с ".$arTimeWork[0]." до ".$arTimeWork[1]);
        }

        return $output;
    }

    /**
     * FormatListHours
     * Форматирование времени на неделю
     *
     * @param string
     * @return string
     */
    function FormatListHours($string)
    {
        $arTime = split(";", mb_strtolower(trim($string)));

        foreach ($arTime as $time)
            $output .= ($output)? "<br>".trim($time) : trim($time);

        return $output;
    }

    /**
     * GetCity
     * Получение города по коду
     *
     * @param string
     * @return integer
     */
    static function GetCity($code)
    {
        CModule::IncludeModule("iblock");
        if(isset($code))
        {
            $result = CIBlockSection::GetList(
                array(),
                array(
                    "IBLOCK_ID" => 5,
                    "CODE" => $code
                ),
                false,
                array("ID", "NAME", "CODE", "UF_VK_GROUP")
            );

            if($arItem = $result->GetNext())
                return $arItem;
            else
                return 0;
        }
        else
            return 0;
    }

    static function Translite($string) {
        $converter = array(
            "а" => "a",   "б" => "b",   "в" => "v",
            "г" => "g",   "д" => "d",   "е" => "e",
            "ё" => "e",   "ж" => "zh",  "з" => "z",
            "и" => "i",   "й" => "y",   "к" => "k",
            "л" => "l",   "м" => "m",   "н" => "n",
            "о" => "o",   "п" => "p",   "р" => "r",
            "с" => "s",   "т" => "t",   "у" => "u",
            "ф" => "f",   "х" => "h",   "ц" => "c",
            "ч" => "ch",  "ш" => "sh",  "щ" => "sch",
            "ь" => "",    "ы" => "y",   "ъ" => "",
            "э" => "e",   "ю" => "yu",  "я" => "ya",

            "А" => "A",   "Б" => "B",   "В" => "V",
            "Г" => "G",   "Д" => "D",   "Е" => "E",
            "Ё" => "E",   "Ж" => "Zh",  "З" => "Z",
            "И" => "I",   "Й" => "Y",   "К" => "K",
            "Л" => "L",   "М" => "M",   "Н" => "N",
            "О" => "O",   "П" => "P",   "Р" => "R",
            "С" => "S",   "Т" => "T",   "У" => "U",
            "Ф" => "F",   "Х" => "H",   "Ц" => "C",
            "Ч" => "Ch",  "Ш" => "Sh",  "Щ" => "Sch",
            "Ь" => "",    "Ы" => "Y",   "Ъ" => "",
            "Э" => "E",   "Ю" => "Yu",  "Я" => "Ya",
        );

        return strtr($string, $converter);
    }

    static function StrToUrl($string) {
        $string = self::Translite($string);
        $string = strtolower($string);
        $string = preg_replace('~[^-a-z0-9_]+~u', "-", $string);
        $string = trim($string, "-");

        return $string;
    }

    public static function PHP2JSON($arr, $asArray = false){
        $html = array();
        $is_object = true;

        foreach($arr as $k=>$v){

            if(is_array($v)){

                if(count($v)){

                    // если все ключи целочисленные, то генерировать Array, иначе - Object
                    $allNum = true;
                    foreach($v as $key => $none)
                        if(!is_int($key)){
                            $allNum = false;
                            break;
                        }

                    $v = self::PHP2JSON($v, $allNum);

                }else
                    $v = '[]';

            }else{

                if($v === 0)
                    $v = 0;
                elseif(is_bool($v))
                    $v = $v ? 'true' : 'false';
                elseif(empty($v))
                    $v = '""';
                elseif(is_string($v))
                    $v = '"'.str_replace(array('"', "\r", "\n"), array('\"', "\\r", "\\n"), $v).'"';
            }

            $html[] = $asArray ? $v : '"'.$k.'"'.':'.$v;
        }

        return ($asArray ? "[" : "{").implode(',', $html).($asArray ? "]" : "}");
    }

    // PxFunc::DefinitionGenus("заведение", array("предоставил", "придоставила", "придоставило"));
    // return "придоставило"
    public static function DefinitionGenus($string, $arString) {
        $arEndings = array(
            array("б", "в", "г", "д", "ж", "з", "и", "й", "к", "л", "м", "н", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ"),
            array("а", "ь", "э", "я"),
            array("е", "ё", "о", "ю")
        );

        $output = $arString[0];
        foreach ($arEndings as $key => $value)
            if(array_search(strtolower(substr($string, -1)), $value) !== false)
                $output = $arString[$key];

        return $output;
    }
}
?>